import React from 'react'
import {useState, useEffect} from 'react';

import { Form, Button } from 'react-bootstrap';

import { Link  } from 'react-router-dom';


import AdminProducts from '../components/AdminProducts'

export default function AdminDashBoard() {

  const [products, setProducts] = useState([]);
  useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/active`)
		.then(res => res.json())
		.then(data => {
			setProducts(data.map(products => {
				return (
					<AdminProducts key = {products._id} product={products}/>
          
				)
			}))
		})
	}, [])
  return (
    <Form className="mx-auto mt-5 mx-auto col-4">
    <h2 >Admin Dashboard</h2>
    <Button variant="info"as={Link} to={`/create`} >Add Orders</Button>
    <Button variant="info" as={Link} to={`/viewUser`}>Show User Orders</Button> 
  
       {products}
    </Form>
  )
}
