
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';


export default function Home(){
  // const data = {
  //   title: "E-Commerce API",
  //   content: "Building E-Commerce API with MERN Stack",
  //   destination: '/product',
  //   label: "Order now"
  // }
  return(
    <>
    <Banner/>
    <Highlights/>
    </>
  );
}
