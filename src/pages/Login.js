import { useState, useEffect, useContext } from 'react';

import {Navigate} from 'react-router-dom';

import { Form, Button } from 'react-bootstrap';


import Swal from 'sweetalert2';


import UserContext from '../UserContext';


function Login() {
  const {user, setUser} = useContext(UserContext);
  const [isActive, setIsActive] = useState(true);
  const [formData,setFormData] = useState({email:''
  ,password:''});
  
  const AppUrl = process.env.REACT_APP_API_URL;

  const authenticate = (e) => {
    e.preventDefault()
      fetch(`${AppUrl}/users/login`, {
      method: 'POST',
      headers: {
          'Content-Type': 'application/json'
      },
      body: JSON.stringify(formData)
      })
      .then(res => res.json())
      .then(data => {
      console.log(data);
      if(typeof data.access !== "undefined") {
        localStorage.setItem('token', data.access); 
        retrieveUserDetails(data.access);

        Swal.fire({
            title: "Login Successful",
            icon: "success",
            text: "Welcome to My E-Commerce!"
        })
    } 
    else {
            Swal.fire({
            title: "Authentication Failed",
            icon: "error",
            text: "Please, check your login details and try again."
        })
    };

      });

    };
    const retrieveUserDetails = (token) => {
      fetch(`${AppUrl}/users/details`, {
          headers: {
              Authorization: `Bearer ${token}`
          }
      })
      .then(res => res.json())
      .then(data => {
          console.log(data);
          setUser({
              id: data._id,
              isAdmin: data.isAdmin

          })
      })
  };

  console.log(formData.email)

  useEffect(() => {
   
    if(formData.email !== '' && formData.password !== ''){
        setIsActive(true);
    }else{
        setIsActive(false);
    }
}, [formData]);
  return (  
     (user.id  !== null) ?
    <Navigate to ="/" />
    :
   
   <Form onSubmit={(e) => authenticate(e)} className="mx-auto mt-5 mx-auto col-4">
   {/* <span>{JSON.stringify(formData,null,2)}</span> for checking */}
      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control type="email"
                    placeholder="Enter your Email:" 
                    value={formData.email}
                    onChange={e => setFormData({...formData, email:e.target.value})}
                    required/>
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicPassword">
        <Form.Label>Password:</Form.Label>
        <Form.Control type="password"
                    placeholder="Password" 
                    value={formData.password}
                    onChange={e => setFormData({...formData, password:e.target.value})}
                    required />
      </Form.Group>
      { isActive ? 
                <Button variant="info" type="submit" id="submitBtn">
                Submit
                </Button>
                :
                <Button variant="danger" type="submit" id="submitBtn" disabled>
                Submit
                </Button>
                }
    </Form>
  
  );
  
}

export default Login;
