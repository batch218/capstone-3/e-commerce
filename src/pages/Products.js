import ProductCard from '../components/ProductCard';

import {useState, useEffect} from 'react';

export default function Products() {
	const [products, setProducts] = useState([]);
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/active`)
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setProducts(data.map(products => {
				return (
					<ProductCard key={products._id} products={products} />
				)
			}))
		})
	}, [])
	return(
		<>
		{products}
		</>
	)
}