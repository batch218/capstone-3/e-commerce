
import { useState, useEffect,useContext } from 'react';
import React from 'react'

import {useNavigate,Navigate} from 'react-router-dom';

import Swal from 'sweetalert2';

import { Form, Button } from 'react-bootstrap';

import UserContext from '../UserContext';

export default function Register() {
  const { user } = useContext(UserContext);
  const navigate = useNavigate(); 
  const [isActive, setIsActive] = useState(false);

  const AppUrl = process.env.REACT_APP_API_URL;

  const [formData,setFormData] = useState({email:'',password1:'',password2:''});

  const registerUser = (e) => {
    e.preventDefault()
   
    fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
      method: "POST",
      headers: {
          'Content-Type': 'application/json'
      },
      body: JSON.stringify({
          email: formData.email
      })
  }).then(res => res.json())
  .then(data => {
      console.log(data)

      if (data === true) {
          Swal.fire({
              title: "Duplicate Email Found",
              icon: "error",
              text: "Kindly provide another email to complete registration."
          })
      } else {
      fetch(`${AppUrl}/users/register`, {
      method: 'POST',
      headers: {
          'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: formData.email,
        password: formData.password1
      })
      })
        .then(res => res.json())
        .then(data => {
        console.log(data);
        
        Swal.fire({
          position: 'top-end',
          icon: 'success',
          title: 'Your have been registered',
          showConfirmButton: false,
          timer: 1500
        })
        navigate("/login");
      });
      }
    })
  }

  useEffect(() => {
    
    if((formData.email !== '' && formData.password1 !== '' && formData.password2 !== '') && (formData.password1 === formData.password2)){
        setIsActive(true);
    } else {
        setIsActive(false);
    }

}, [formData])
console.log(user.id)
  return (
 
    (user.id !== null) ?
    <Navigate to ="/login" />
    : 
       <Form  onSubmit={(e) => registerUser(e)} className="mx-auto mt-5 mx-auto col-4">   
      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control type="text"
                    placeholder="Enter Email" 
                    value={formData.email}
                    onChange={e => setFormData({...formData, email:e.target.value})}
                    required /> 
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicPassword">
        <Form.Label>Password:</Form.Label>
        <Form.Control type="password"
                    placeholder="Password" 
                    value={formData.password1}
                    onChange={e => setFormData({...formData, password1:e.target.value})}
                    required />
      </Form.Group>
     <Form.Group className="mb-3" controlId="formBasicPassword ">
        <Form.Label>Password:</Form.Label>
        <Form.Control type="password"  
                    placeholder="Verify Password" 
                    value={formData.password2}
                    onChange={e =>  setFormData({...formData, password2:e.target.value})}
                    required
 />
      </Form.Group>
      
      { isActive ? 
                <Button variant="info" type="submit" id="submitBtn">
                Submit
                </Button>
                :
                <Button variant="danger" type="submit" id="submitBtn" disabled>
                Submit
                </Button>
                }
    </Form>
  )
}
