
import { useState, useEffect } from "react"

import {Container} from 'react-bootstrap';

import Home from './pages/Home'
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';

import {UserProvider} from './UserContext';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'

import ProductView from "./components/ProductView";
import NavBar from "./components/NavBar";
import AddProduct from "./components/addProduct";
import ViewUsers from './components/ViewUser'
import UpdateProduct from './components/UpdateProduct'


import './App.css';
import AdminDashBoard from "./pages/AdminDashBoard";
import Products from './pages/Products';





function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });
  const unsetUser = () => {
    localStorage.clear();
  }
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
        headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`
        }
    })
    .then(res => res.json())
    .then(data => {
        // user is logged in
        if(typeof data._id !== "undefined") {

            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        } 
        // User is logged out
        else { 
            setUser({
                id: null,
                isAdmin: null
            })
        }
    })
}, []);

  return (
    <>
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
      <NavBar/>  
      <Container>
          <Routes>
            <Route path="/" element={<Home/>}/>
            <Route path="/products" element={<Products/>}/>
            <Route path="/register" element={<Register/>}/>
            <Route path="/products/:productId" element={<ProductView />} />
            <Route path="/:productId/update" element={<UpdateProduct />} />
            <Route path="/viewUser" element={<ViewUsers />} />
            <Route path="/create" element={<AddProduct />} />
            <Route path="/admindashboard" element={<AdminDashBoard/>}/>
            <Route path="/login" element={<Login/>}/>
            <Route path="/logout" element={<Logout />} />
          </Routes>
          </Container> 
      </Router>
      </UserProvider>
    </>

  );
}

export default App;



