import React from 'react';
import { useContext} from 'react';

import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';

import UserContext from '../UserContext';



import { Link, NavLink } from 'react-router-dom'
function NavBar() {
  const { user } = useContext(UserContext);
  // const { isAdmin } = useContext(UserContext);
  return (
    <Navbar bg="info" expand="lg" >
    <Navbar.Brand as={Link} to="/">E-Commerce API</Navbar.Brand>
    <Navbar.Toggle aria-controls="basic-navbar-nav" />
    <Navbar.Collapse id="basic-navbar-nav">
      <Nav className="ml-auto ">
        <Nav.Link as={NavLink} to="/">Home</Nav.Link>
        
        {(user.isAdmin === false)?
          <Nav.Link as={NavLink} to="/products">Products</Nav.Link>
        :
        <>
        
        </>
        }
        {(user.id !== null) ?
        <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
        :
        <>
        <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
        <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
        </>
        }
        {(user.isAdmin === true)?
          <Nav.Link as={NavLink} to="/admindashboard">AdminDashBoard</Nav.Link>
        :
        <>
        
        </>
        }
      
      </Nav>
    </Navbar.Collapse>
  </Navbar>

  );
}

export default NavBar;

