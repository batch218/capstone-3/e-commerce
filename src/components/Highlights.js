import {Row,Col,Card} from 'react-bootstrap'


export default function Highlights() {
	return (
	    <Row className="mt-3 mb-3">
	        <Col xs={12} md={4}>
	            <Card className="cardHighlight p-3">
	                <Card.Body>
	                    <Card.Title>
	                        <h2>Shop on Online, Order now </h2>
	                    </Card.Title>
	                    <Card.Text>
	                        Enjoy less hassle and covid-free shopping. Here in our shopping online we valued costumer satisfaction enjoy shopping.

	                    </Card.Text>
	                </Card.Body>
	            </Card>
	        </Col>
	        <Col xs={12} md={4}>
	            <Card className="cardHighlight p-3">
	                <Card.Body>
	                    <Card.Title>
	                        <h2>We got it all for you </h2>
	                    </Card.Title>
	                    <Card.Text>
	                        Here in our Shopping mart. We have the thing that you've needed. Just search it or even we recommend things that you've really wanted to shop.
	                    </Card.Text>
	                </Card.Body>
	            </Card>
	        </Col>
	        <Col xs={12} md={4}>
	            <Card className="cardHighlight p-3">
	                <Card.Body>
	                    <Card.Title>
	                        <h2>Just add to cart and Order now</h2>
	                    </Card.Title>
	                    <Card.Text>
	                        Order now or Order later. Just click the order button or add it to cart. Enjoy more vouchers and fast delivery. Just wait till it get to your house.
	                    </Card.Text>
	                </Card.Body>
	            </Card>
	        </Col>
	    </Row>
	)
}