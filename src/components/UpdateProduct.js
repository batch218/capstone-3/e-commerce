import { useState } from 'react';

import { Form, Button } from 'react-bootstrap';

import Swal from 'sweetalert2';

import { Link,useParams,useNavigate } from 'react-router-dom';

export default function UpdateProduct() {
  const { productId } = useParams();
  const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState('');
  const navigate = useNavigate();
  console.log(productId)
  const updateProducts = (productId) => {
    console.log("name")
    console.log("fetch")
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/update`, {
			method: "PATCH",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
        name: name,
				description:description,
				price: price
				
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
      Swal.fire({
        title: "Successfully update",
        icon: "success",
        text: "You have successfully updated a product."
      })
      navigate("/admindashboard")
		})
	};

  return (
   
    <Form className="mx-auto mt-5 mx-auto col-4">   
    <Form.Group className="mb-3" controlId="name">
      <Form.Label>Name:</Form.Label>
      <Form.Control type="text"
                    placeholder="Enter Name of the Product" 
                    value={name}
                    onChange={e => setName(e.target.value)}
                    required/> 
    </Form.Group>
    <Form.Group className="mb-3" controlId="description">
      <Form.Label>Description:</Form.Label>
      <Form.Control type="text"
                    placeholder="Enter Description" 
                    value={description}
                    onChange={e => setDescription(e.target.value)}
                    required/> 
    </Form.Group>
    <Form.Group className="mb-3" controlId="price">
      <Form.Label>Price:</Form.Label>
      <Form.Control  type="text"
                    placeholder="Enter Price" 
                    value={price}
                    onChange={e => setPrice(e.target.value)}
                    required/> 
    </Form.Group>
    <Button variant="primary" onClick={(e) => {updateProducts(productId)}} >Save</Button>
    <Button variant="primary" as={Link} to={`/admindashboard`} >View Order</Button>
    </Form> 
  )
}
