import React from 'react'
import {useState, useEffect} from 'react';

import { Form, Button } from 'react-bootstrap';
import Table from 'react-bootstrap/Table';

import { Link  } from 'react-router-dom';


export default function ViewUser() {
  const  [nonAdminUsers, setNonAdminUsers] = useState([]);

  useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/users/All`)
		.then(res => res.json())
		.then(data => {
			setNonAdminUsers(data)
		})
	}, [])

	
  return (
		<Form className="mx-auto mt-5 mx-auto col-4">
		<h1>Non-Admin User</h1>
		<Table striped bordered hover>
     <thead >
				<tr>  
          <td>Email:</td>
					<td>Orders:</td>
        </tr>
			</thead>
			<tbody >
			{ nonAdminUsers.length && 
						nonAdminUsers.map(user => 
				<tr key = {user._id}>
          <td>{user.email}</td>                
					<td>{user.orders.length}</td> 
        </tr> )
 			}
					   
				</tbody>   
    </Table>
		<Button variant="warning" as={Link} to={`/admindashboard`}>Back to Admin Dashboard</Button>
		</Form>
  )
}
