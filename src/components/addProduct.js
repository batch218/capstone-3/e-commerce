// import React from 'react'
import { useState } from 'react';

import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { Link,useNavigate } from 'react-router-dom';


export default function AddProduct() {
  const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState('');
  const navigate = useNavigate();
  const createOrder = (productId) => {
		fetch(`${process.env.REACT_APP_API_URL}/products/create`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
        name: name,
				description:description,
				price: price
				
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
      if(data !== null) {
				Swal.fire({
					title: "Successfully Added order",
					icon: "success",
					text: "You have successfully added order."
				})
        navigate("/admindashboard")
      }
		})
	};

  return (
   
    <Form onSubmit={(e) => createOrder(e)} className="mx-auto mt-5 mx-auto col-4">   
    <Form.Group className="mb-3" controlId="name">
      <Form.Label>Name:</Form.Label>
      <Form.Control type="text"
                    placeholder="Enter Name of the Product" 
                    value={name}
                    onChange={e => setName(e.target.value)}
                    required/> 
    </Form.Group>
    <Form.Group className="mb-3" controlId="description">
      <Form.Label>Description:</Form.Label>
      <Form.Control type="text"
                    placeholder="Enter Description" 
                    value={description}
                    onChange={e => setDescription(e.target.value)}
                    required/> 
    </Form.Group>
    <Form.Group className="mb-3" controlId="price">
      <Form.Label>Price:</Form.Label>
      <Form.Control  type="text"
                    placeholder="Enter Price" 
                    value={price}
                    onChange={e => setPrice(e.target.value)}
                    required/> 
    </Form.Group>
    <Button variant="primary"onClick={() => createOrder()} >Save</Button>
    <Button variant="primary"as={Link} to={`/admindashboard`} >View Order</Button>
    </Form> 
   
  )
}
