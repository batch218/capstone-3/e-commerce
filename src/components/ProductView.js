import { useState, useEffect, useContext } from 'react';
import { Form, Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useNavigate, Link  } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductView() {
	const navigate = useNavigate();
	const {productId } = useParams();
	const [orderQuantity,setOrderQuantity] = useState(1);
	const { user } = useContext(UserContext);
	
	const [productDetails,setProductDetails] = useState({
		productId:productId,name:'',description:'',price:''
	})

	const createOrder = (productId) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/createOrder`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId:productId,
				quantity:orderQuantity
				
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data === true) {
				Swal.fire({
					title: "Successfully order",
					icon: "success",
					text: "You have successfully ordered."
				})

				navigate("/products")

			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}

		})
	};

	useEffect(() => {

		console.log(productId);

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setProductDetails({productId:data._id,name:data.name,description:data.description,price:data.price});
		})
	}, [productId])


	return (

		<Container>
			<Row>
				<Col lg={{span: 6, offset:3}} >
					<Card>
					      <Card.Body className="text-center">
					        <Card.Title>{productDetails.name}</Card.Title>
					        <Card.Subtitle>Description:</Card.Subtitle>
					        <Card.Text>{productDetails.description}</Card.Text>
					        <Card.Subtitle>Price:</Card.Subtitle>
					        <Card.Text>PhP {productDetails.price}</Card.Text>
					        <Form.Group className="mb-3" controlId="formBasicPassword">
        <Form.Label>Quantity:</Form.Label>
        <Form.Control type="number"
                    placeholder="Quantity" 
                    value={orderQuantity}
                    onChange={e => setOrderQuantity(e.target.value)}
                    required />
      </Form.Group>
					        { 
					        	(user.id !== null) ?
					        		<Button variant="info" onClick={() => createOrder(productDetails.productId)} >Order Now</Button>
					        		:
					        		<Button className="btn btn-danger" as={Link} to="/login"  >Log in to Order</Button>
					        }

					      </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>

	)
}