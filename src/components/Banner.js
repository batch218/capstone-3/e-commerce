
import {Button,Row,Col} from 'react-bootstrap'
import {Link} from 'react-router-dom'
import { useContext} from 'react';

import UserContext from '../UserContext';

export default function Banner() {
  const { user } = useContext(UserContext);
  // const  {title, content, destination, label} = data;
console.log(user)
  return (
    
         <Row>
        <Col className="p-5">
              <h4>E-Commerce API</h4>
              <p>Building E-Commerce API with MERN Stack</p>
              {(!user.isAdmin)?
              <Button variant= "info" as={Link} to={`/products`}>Order now</Button>
              :
              <Button variant= "info" as={Link} to={`/admindashboard`}>Go to Admin Dashboard</Button>
              }
          </Col>
      </Row>
            
    )
  }

 