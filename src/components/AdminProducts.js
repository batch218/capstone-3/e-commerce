import React from 'react'

import { Card, Button } from 'react-bootstrap';

import Swal from 'sweetalert2';

import { useParams, useNavigate,Link  } from 'react-router-dom';


export default function AdminProducts({product}) {
  const { productId } = useParams();
  const { name, description, price, _id } = product;

  const navigate = useNavigate();
function softDelete(productId){
  console.log(product._id)
  fetch(`${process.env.REACT_APP_API_URL}/products/${product._id}/archive`, {
    method: "PATCH",
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${localStorage.getItem('token')}`
    },
    body: JSON.stringify({
      productId:productId
      
    })
  })
  .then(res => res.json())
  .then(data => {
    console.log(data)
    if(data !== null) {
      Swal.fire({
        title: "Successfully archive",
        icon: "success",
        text: "You have successfully unarchived a product."
      })
      navigate("/admindashboard")
    }
  })
}
function unarchive(productId){
  console.log(product._id)
  fetch(`${process.env.REACT_APP_API_URL}/products/${product._id}/archive`, {
    method: "PATCH",
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${localStorage.getItem('token')}`
    },
    body: JSON.stringify({
      productId:productId
      
    })
  })
  .then(res => res.json())
  .then(data => {
    console.log(data)
    if(data !== null) {
      Swal.fire({
        title: "Successfully Unarchive",
        icon: "success",
        text: "You have successfully unarchived a product."
      })
      navigate("/admindashboard")
    }
  })
}

  return (
    <Card>
    <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle>Description:</Card.Subtitle>
        <Card.Text>{description}</Card.Text>
        <Card.Subtitle>Price:</Card.Subtitle>
        <Card.Text>PhP {price}</Card.Text>
        <Button className="bg-info" as={Link} to={`/${_id}/update`} >Update</Button>
        <Button className="bg-danger" onClick={(e) => {softDelete(productId)}} >Archive</Button>
         <Button className="bg-success" onClick={(e) => {unarchive(productId)}} >Unarchive</Button>
    </Card.Body>
</Card>
        
  )
}